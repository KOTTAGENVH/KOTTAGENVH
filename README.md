### Hi there 👋, Nowen Kottage
#### Development

![](https://komarev.com/ghpvc/?username=KOTTAGENVH&style=for-the-badge-square)

👋 Hi, I’m @NOWEN-KOTTAGE

👀 I’m interested in Swift, Java, Java Script and Python


![Top Langs](https://github-readme-stats.vercel.app/api/top-langs/?username=KOTTAGENVH&hide_progress=false)

[![Nowen's GitHub stats](https://github-readme-stats.vercel.app/api?username=KOTTAGENVH)](https://github.com/anuraghazra/github-readme-stats)
